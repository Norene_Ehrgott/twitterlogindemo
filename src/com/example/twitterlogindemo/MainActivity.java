package com.example.twitterlogindemo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Register your here app https://dev.twitter.com/apps/new and get your consumer
 * key and secret
 * */
public class MainActivity extends ActionBarActivity implements OnClickListener {

	
	/*static final String TWITTER_CONSUMER_KEY ="t438xJ7yjwTWn7MeW2tH9Q";
	static final String TWITTER_CONSUMER_SECRET ="AVtSkceC0eB4eiTYRXJzemqJTRNhF2JWGV9Ax5K0Pw0";*/
	
	private static final String TAG = "TWITTER_LOGIN";
	
	private static final String TWITTER_CONSUMER_KEY = "YOUR CONSUMER KEY";
	private static final String TWITTER_CONSUMER_SECRET = "YOUR CONSUMER SECRET KEY";
	protected static final String AUTHENTICATION_URL_KEY = "AUTHENTICATION_URL_KEY";
	private static final int LOGIN_TO_TWITTER_REQUEST = 0;

	// Preference Constants
	static String PREFERENCE_NAME = "twitter_oauth";
	static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
	static final String PREF_KEY_USER_NAME = "username";

	static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";

	// Twitter oauth urls
	static final String URL_TWITTER_AUTH = "auth_url";
	static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
	// Login button
	Button btnLoginTwitter;
	// Update status button
	Button btnUpdateStatus;
	// Logout button
	Button btnLogoutTwitter;
	// EditText for update
	EditText txtUpdate;
	// lbl update
	TextView lblUpdate;
	TextView lblUserName;
	private static Twitter twitter;
	private static RequestToken requestToken;
	ImageView imageView;
	// Shared Preferences
	private static SharedPreferences mSharedPreferences;

	// Internet Connection detector
	private ConnectionDetector cd;
	Intent pictureIntent = null;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	// Progress dialog
	ProgressDialog pDialog;
	private String imgFilePath = "";
	private final int GALLERY_PICTURE = 10;
	private final int CAMERA_PICTURE = 11;
	Menu menu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		cd = new ConnectionDetector(MainActivity.this);
		// Check if Internet present
		if (!cd.isConnectingToInternet()) {
			// Internet Connection is not present
			alert.showAlertDialog(MainActivity.this,
					"Internet Connection Error",
					"Please connect to working Internet connection", false);
			// stop executing code by return
			return;
		}

		// Check if twitter keys are set
		if (TWITTER_CONSUMER_KEY.trim().length() == 0
				|| TWITTER_CONSUMER_SECRET.trim().length() == 0) {
			// Internet Connection is not present
			alert.showAlertDialog(MainActivity.this, "Twitter oAuth tokens",
					"Please set your twitter oauth tokens first!", false);
			// stop executing code by return
			return;
		}
		initUi();

	}

	@Override
	protected void onResume() {
		updateUI();
		super.onResume();
	}

	private void initUi() {
		btnLoginTwitter = (Button) findViewById(R.id.btnLoginTwitter);
		btnUpdateStatus = (Button) findViewById(R.id.btnUpdateStatus);
		btnLogoutTwitter = (Button) findViewById(R.id.btnLogoutTwitter);

		txtUpdate = (EditText) findViewById(R.id.txtUpdateStatus);
		lblUpdate = (TextView) findViewById(R.id.lblUpdate);
		lblUserName = (TextView) findViewById(R.id.lblUserName);
		imageView = (ImageView) findViewById(R.id.imageView1);
		
		mSharedPreferences = getApplicationContext().getSharedPreferences(
				"MyPref", MODE_PRIVATE);

		btnLoginTwitter.setOnClickListener(this);   // call loginToTwitter() function .
		btnLogoutTwitter.setOnClickListener(this); // Perform Twitter logout operation. 
		btnUpdateStatus.setOnClickListener(this); // Perform Twitter Status update with text or image.

	}

	protected void loginToTwitter() {
		
		if (!isTwitterAlreadyLogedin()) {
			
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
			builder.setDebugEnabled(true);
			Configuration confrigation = builder.build();
			
			TwitterFactory factory = new TwitterFactory(confrigation);
			twitter = factory.getInstance();
			
			try {
				requestToken = twitter
						.getOAuthRequestToken(TWITTER_CALLBACK_URL);
			
				launchLoginWebView(requestToken);
				requestToken = null;

			} catch (TwitterException e) {
				
				e.printStackTrace();
			}
		} else {
			
			Log.d(TAG, "Already Logged into twitter");
		}

	}

	private void launchLoginWebView(RequestToken requestToken) {
		
		Intent intent = new Intent(MainActivity.this, LoginToTwitter.class);
		
		Log.d(TAG,"Authenticate URL "+ requestToken.getAuthenticationURL());
		
		intent.putExtra(AUTHENTICATION_URL_KEY,
				requestToken.getAuthenticationURL());
		startActivityForResult(intent, LOGIN_TO_TWITTER_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (resultCode == Activity.RESULT_OK) {
			
			switch (requestCode) {
			
			case LOGIN_TO_TWITTER_REQUEST:
		
				try {

					if (!isTwitterAlreadyLogedin()) {
						Uri uri = Uri
								.parse(data
										.getStringExtra(LoginToTwitter.CALLBACK_URL_KEY));
						Log.d(TAG , "Uri -- " +uri.toString());
						
						if (uri != null && uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
							
							// oAuth verifier
							String verifier = uri
									.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
							
							Log.d(TAG,"verifier == " + verifier);
							
							new GetAccessToken().execute(verifier);
						}
					}

				} catch (Exception e) {
					
					e.printStackTrace();
				}

				break;
			case GALLERY_PICTURE:

				imageFromGallery(resultCode, data);
				break;
			case CAMERA_PICTURE:
				
				imageFromCamera(resultCode, data);
				break;
			default:
				break;
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private class LoginToTwitterThread extends AsyncTask<Void, Void, Void> {

		ProgressDialog pdialog;

		@Override
		protected void onPreExecute() {
		
			pdialog = new ProgressDialog(MainActivity.this);
			pdialog.setMessage("Please wait...");
			pdialog.setCancelable(false);
			pdialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			
			loginToTwitter();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			
			if (pdialog != null) {
			
				if (pdialog.isShowing()) {
				
					pdialog.dismiss();
				}
			}
			super.onPostExecute(result);
		}
	}

	class GetAccessToken extends AsyncTask<String, Void, Long> {
		
		ProgressDialog pdialog;
		String username;

		@Override
		protected void onPreExecute() {
		
			pdialog = new ProgressDialog(MainActivity.this);
			pdialog.setMessage("Please wait...");
			pdialog.setCancelable(false);
			pdialog.show();
			
			super.onPreExecute();
		}

		@Override
		protected Long doInBackground(String... params) {
			
			String verifier = params[0];
			long userID = 0;
			
			try {
			
				// Get the access token
				AccessToken accessToken = twitter.getOAuthAccessToken(verifier);
			
				try {
				
					username = twitter.getScreenName();
				
				} catch (Exception e) {
					
					e.printStackTrace();
				}

				// Shared Preferences
				Editor e = mSharedPreferences.edit();
				// After getting access token, access token secret
				// store them in application preferences
				e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
				e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
				// Store login status - true
				e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
				e.putString(PREF_KEY_USER_NAME, username);
				e.commit(); // save changes
				userID = accessToken.getUserId();
				
				Log.d(TAG , "Twitter accessToken == "+ accessToken.getToken());
				Log.d(TAG , "Twitter Secret Token == "+ accessToken.getTokenSecret());
				Log.d(TAG , "UserName  == "+ username);
				Log.d(TAG , "UserID == "+ userID);

			} catch (Exception e) {
				
				Log.e(TAG, "Twitter login Error");
				e.printStackTrace();
			}
			return userID;
		}

		@Override
		protected void onPostExecute(Long result) {
			
			if (pdialog != null) {
				
				if (pdialog.isShowing()) {
					
					// Hide login button and updating Ui
					updateUI();

					pdialog.dismiss();
				}
			}
			super.onPostExecute(result);
		}
	}

	class UpdateTwitterStatus extends AsyncTask<String, String, String> {
		
		ProgressDialog pdialog;

		@Override
		protected void onPreExecute() {
			
			pdialog = new ProgressDialog(MainActivity.this);
			pdialog.setMessage("Updating status...");
			pdialog.setCancelable(false);
			pdialog.show();

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			
			try {
			
				String straccessToken = mSharedPreferences.getString(
						PREF_KEY_OAUTH_TOKEN, "");
				String straccessTokenSecret = mSharedPreferences.getString(
						PREF_KEY_OAUTH_SECRET, "");

				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
				builder.setDebugEnabled(true);
				builder.setOAuthAccessToken(straccessToken);
				builder.setOAuthAccessTokenSecret(straccessTokenSecret);
				
				Configuration confrigation = builder.build();
				AccessToken access_Token = new AccessToken(straccessToken,
														straccessTokenSecret);

				TwitterFactory factory = new TwitterFactory(confrigation);
				Twitter twitter1 = factory.getInstance(access_Token);

				StatusUpdate statusUpdate = new StatusUpdate(params[0]);
				
				if (imgFilePath.length() > 0) {
					
					Log.d(TAG,"Image FilePath == "+ imgFilePath.length());
					
					statusUpdate.setMedia(new File(imgFilePath));
				}

				twitter4j.Status status = twitter1.updateStatus(statusUpdate);

				Log.d(TAG,status.getText());
				return status.getText();

			} catch (TwitterException e) {

				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(String result) {
			
			if (pdialog != null) {
				
				if (pdialog.isShowing()) {
					
					pdialog.dismiss();
				}
				if (result != null) {
					
					Toast.makeText(MainActivity.this, "Twitter Status Updated Successfully", Toast.LENGTH_SHORT).show();
				}
			}
			super.onPostExecute(result);
		}

	}

	private void updateUI() {
		
		// Getting user details from twitter
		// Displaying in xml ui
		if (isTwitterAlreadyLogedin()) {
			
			btnLoginTwitter.setVisibility(View.GONE);
			lblUpdate.setVisibility(View.VISIBLE);
			txtUpdate.setVisibility(View.VISIBLE);
			btnUpdateStatus.setVisibility(View.VISIBLE);
			btnLogoutTwitter.setVisibility(View.VISIBLE);
			lblUserName.setVisibility(View.VISIBLE);
			
			lblUserName.setText(Html.fromHtml("<b>Welcome "
					+ mSharedPreferences.getString(PREF_KEY_USER_NAME, "")
					+ "</b>"));

			if (menu != null) {
				
				menu.findItem(R.id.action_settings).setVisible(true);
			}
			
		} else {
			
			btnLoginTwitter.setVisibility(View.VISIBLE);
			lblUserName.setVisibility(View.GONE);
			txtUpdate.setText("");
			lblUpdate.setVisibility(View.GONE);
			txtUpdate.setVisibility(View.GONE);
			btnUpdateStatus.setVisibility(View.GONE);
			btnLogoutTwitter.setVisibility(View.GONE);
			
			imageView.setImageDrawable(null);
			imageView.setVisibility(View.GONE);
			
			if (menu != null) {
			
				menu.findItem(R.id.action_settings).setVisible(false); // Hiding the option menu if not logged In 
			}
		}
	}

	public boolean isTwitterAlreadyLogedin() {
		
		return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		this.menu = menu;
		
		if (!isTwitterAlreadyLogedin()) {
			
			this.menu.findItem(R.id.action_settings).setVisible(false);

		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		takeImage();
		
		return super.onOptionsItemSelected(item);
	}

	private void takeImage() {

		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setTitle("Image Chooser");
		builder.setMessage("Choose Image from resources");
		
		builder.setPositiveButton("Galery",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						pictureIntent = new Intent(Intent.ACTION_PICK,
								MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						pictureIntent.setType("image/*");
						pictureIntent.putExtra("return-data", true);
						startActivityForResult(pictureIntent, GALLERY_PICTURE);
						startActivity(pictureIntent);
					}
				});
		
		builder.setNegativeButton("Camera",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						pictureIntent = new Intent(
								MediaStore.ACTION_IMAGE_CAPTURE);
						startActivityForResult(pictureIntent, CAMERA_PICTURE);
					}
				});
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void imageFromGallery(int resultCode, Intent data) {

		Uri selectedImage = data.getData();

		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		Cursor cursor = getContentResolver().query(selectedImage,
				filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String filePath = cursor.getString(columnIndex);

		cursor.close();
		
		Log.d(TAG,"File Path == " + filePath);
		Log.d(TAG,"Image from Gallarey");

		File imgFile = new File(filePath);

		Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

		imageView.setVisibility(ImageView.VISIBLE);
		imageView.setImageBitmap(myBitmap);

		this.imgFilePath = filePath;
	}

	private void imageFromCamera(int resultCode, Intent data) {

		Bitmap bitmap = (Bitmap) data.getExtras().get("data");
		imageView.setVisibility(ImageView.VISIBLE);
		imageView.setImageBitmap(bitmap);

		Log.d(TAG,"Image from Camera");

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();

		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

		java.util.Date date = new java.util.Date();

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date
				.getTime());

		String filePath = Environment.getExternalStorageDirectory() + "/DCIM/"
				+ "IMG_" + timeStamp + ".png";

		File file = new File(filePath);

		this.imgFilePath = filePath;

		try {
			
			file.createNewFile();
			FileOutputStream fo = new FileOutputStream(file);
			fo.write(bytes.toByteArray());
			fo.close();
			
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.btnLoginTwitter:

			if (!isTwitterAlreadyLogedin()) {

				new LoginToTwitterThread().execute();
			}

			break;

		case R.id.btnLogoutTwitter:

			Editor e = mSharedPreferences.edit();

			e.remove(PREF_KEY_OAUTH_TOKEN);
			e.remove(PREF_KEY_OAUTH_SECRET);
			e.remove(PREF_KEY_TWITTER_LOGIN);
			e.commit();

			updateUI();

			break;

		case R.id.btnUpdateStatus:

			String status = txtUpdate.getText().toString();
			txtUpdate.setText("");

			if (status.trim().length() > 0) {

				new UpdateTwitterStatus().execute(status);
			} else {

				Toast.makeText(MainActivity.this, "Enter Some text",
						Toast.LENGTH_SHORT).show();
			}

			break;

		default:
			break;
		}
	}
}