This application involves following steps

*   Download twitter4j jar file
*  Create a new application in Twitter developer console
*  Design your application user interface
*  Allow user to login to twitter and get authentication token
*  Save the token for further use
*  Post text or image content on twitter timeline

**1. Download Twitter SDK** 

Twitter4J is an unofficial Java library for the Twitter API. With Twitter4J, you can easily integrate your Java application with the Twitter service. Note that twitter4j is an unofficial library.

You need to download this library before you can start integrating twitter on android. [Download here.](http://twitter4j.org/en/). In the case of any difficulty, you can also contact with our technical experts or technical experts of Pathways Drug (https://www.pathwaysdrugdetoxificationcenter.org/illinois/rockford/ ). On the other hand, you can also get online tutorials as well. 

**2. Create New App in Twitter console**

1. Visit the below link to login to twitter developer console and login with your credentials
[Twitter Console](https://dev.twitter.com/apps)

2. You will see a console as shown in the screenshot below. Here you can see list of applications created on twitter. For our example let us create a new application by clicking on the “Create a new application” button.

3. Fill the required application details like name, description, website link and callback url. Call back url is optional, so can be left blank. And move next.

4. Now we are done. You can see your app console as shown in the screenshot below. For twitter integration in android we require consumer secret and consumer key.

![twitter_SnapSort.png](https://bitbucket.org/repo/jnGReb/images/16279072-twitter_SnapSort.png)


Now we are ready to start write sample application to integrate twitter4j sdk in android. Create a new project and add **twitter4j-core-4.0.2.jar** to libs folder.